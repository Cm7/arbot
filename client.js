'use strict'

const WebSocket = require( 'ws' ) ;
const wsURL = 'ws://jglee.net:3000' ;

var socket ;
const clientName = 'ygl' ;
var connectStatus = false ;

function ConnectMServer()
{
    socket = new WebSocket( wsURL ) ;

    socket.on( 'open' , function() {
        connectStatus = true ;
        console.log( 'ConnectMSocket() | Monitoring Server Socket Connection Success!' ) ;
    
        var message = {
            type: 'open' ,
            name: clientName
        }

        socket.send( JSON.stringify(message) , () => {
            console.log( "ConnectMServer() | Open Message Send !" ) ;
        }) ;
    }) ;


    socket.on( 'error' , function( error ) {
        console.log( 'ConnectMSocket() | WebSocket Error ' + error ) ;
        connectStatus = false ;
        // process.exit(1) ;
    }) ;
}

function SendBalance( balance )
{
    var message = {
        type: 'balance' ,
        name: clientName ,
        KRW: balance.KRW ,
        BTC: balance.BTC ,
        total: balance.total ,
        assetCount: balance.assetCount
    } ;

    if( true === connectStatus )
        socket.send( JSON.stringify(message) ) ;
}

function SendAuth( auth )
{
    var message = {
        type: 'auth' ,
        name: clientName ,
        token: auth.token ,
        refreshToken: auth.refreshToken ,
        renewCount: auth.renewCount
    } ;

    if( true === connectStatus )
        socket.send( JSON.stringify(message) ) ;
}

function getConnectStatus( callback )
{
    callback( connectStatus ) ;
    return ;
}   



module.exports = {
    ConnectMServer: ConnectMServer ,
    getConnectStatus: getConnectStatus ,
    SendBalance: SendBalance ,
    SendAuth: SendAuth
} ;

'use strict'

const request = require( 'request' ) ;
const crypto = require( 'crypto' ) ;
const zlib = require( 'zlib' ) ;
const ws = require( 'ws' ) ;
const base64url = require( 'base64url' ) ;

const g_KRW_fee = 0.00051 ;
const g_BTC_fee = 0.00251 ;

var g_priority = 0 ;
var token = 'eyJhcGkiOnsiYWNjZXNzX2tleSI6IjVpVEozTG1zRTcyZVhiUEV4a1llV2VJbHpiSDBSUEc2ekYyaE9UanQiLCJzZWNyZXRfa2V5IjoiVkFpRVV3QlJJZUpBOVFhTTFodWwyQXJFa0NvUkxGbnNqalc2RzhoOSJ9fQ' ;
var refreshToken = '4b197eded645e0d162a655401c4207e3acc19a4219d1ae7a3ceeeb8fbaedec29' ;
var api = { access_key: '' , secret_key: '' } ;
var eTags = { asset: undefined , balance: undefined } ;

var renewCount = 0 ;
var loginState = 0 ;

const jwt_header = { alg: 'HS256' , typ: 'JWT' } ;

var currentBalance = {
    KRW: { balance: 0 , locked: 0 } ,
    BTC: { balance: 0 , locked: 0 , avgPrice: 0 , buyingPrice: 0 , rate: 0 } ,
    total: 0 ,
    assetCount: 0 
}

var coinPriceMap = new Map() ;
var coinProfitMap_phase1 = new Map() ;
var coinProfitMap_phase2 = new Map() ;

function setAPI()
{
    return new Promise( (resolve) => {
        var decryptedToken = JSON.parse( base64url.decode( token ) ) ;
        api = decryptedToken.api ;
        console.log( api ) ;
        console.log( "\n" ) ;
        resolve() ;
    }) ;
}

function getToken()
{
    var payload = {
        access_key: api.access_key ,
        nonce: new Date().getTime()
    } ;

    var encodedHeader = new Buffer(JSON.stringify( jwt_header )).toString( 'base64' )
                                                                .replace(/=+$/, "")
                                                                .replace(/\+/g, "-")
                                                                .replace(/\//g, "_") ;

    var encodedPayload = new Buffer( JSON.stringify( payload ) ).toString( 'base64' )
                                                                .replace(/=+$/, "")
                                                                .replace(/\+/g, "-")
                                                                .replace(/\//g, "_") ;

    var signature = crypto.createHmac( 'sha256' , api.secret_key )
                        .update( encodedHeader + '.' + encodedPayload )
                        .digest( 'base64' )
                        .replace( /=+$/ , "" )
                        .replace( /\+/g , "-" )
                        .replace( /\//g , "_" ) ;
    
    return ( encodedHeader + '.' + encodedPayload + '.' + signature ) ;
} ;

function getRenewCount()
{
    return new Promise( function( resolve ) {
        resolve( renewCount ) ;
    }) ;
}

function getCurrentToken()
{
    return new Promise( function( resolve ) {
        resolve( { token: token , refreshToken: refreshToken , renewCount: renewCount } ) ;
    }) ;
}


function getRequestOption()
{
    var option = {
        method: 'POST' ,
        httpVersion: 'HTTP/1.1' ,
        headers: {
            'host': 'ccx.upbit.com' ,
            'Authorization': 'Bearer ' + getToken() ,
            'Connection': 'keep-alive' ,
            'Origin': 'https://upbit.com' ,
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36' ,
            'Accept': '*/*' ,
            'Accept-Encoding': 'gzip, deflate, br' ,
            'Accept-Language': 'ko,en-US;q=0.9,en;q=0.8,ja;q=0.7' ,
        }
    } ;

    // arguments[0] is request type : REQUEST | VERIFY | ORDER_BID | ORDER_ASK | ORDER_DELETE | ORDER_LIST | ORDER_CHANCE
    // arguments[1] is order or order_chance type : BID | ASK | DELETE | LIST | market( KRW-BTC )
    // arguments[2] is post json form
    switch( arguments[0] )
    {
        case 'REQUEST':
            option.url = 'https://ccx.upbit.com/api/v1/signin/phone_otp_auths/request' ;
            break ;

        case 'VERIFY':
            option.url = 'https://ccx.upbit.com/api/v1/signin/phone_otp_auths/verify?tx_id=' + this._tx_id + '&otp=' + this._otp ;
            break ;

        case 'RENEW':
            option.method = 'PUT' ;
            option.url = 'https://ccx.upbit.com/api/v1/kakao/web/auth?refresh_token=' + refreshToken ;
            option.encoding = null ;
            break ;

        // arguments[1] : uuid
        case 'ORDER_DELETE':
            option.method = 'DELETE' ;
            option.url = 'https://ccx.upbit.com/api/v1/order?uuid=' + arguments[1] ;
            option.encoding = null ;
            break ;

        // arguments[1] : order form
        case 'ORDER':
            option.url = 'https://ccx.upbit.com/api/v1/orders' ;
            option.headers[ 'Content-Type' ] = 'application/json' ;
            option.form = arguments[1] ;
            break ;

        case 'ORDER_LIST':
            option.method = 'GET' ;
            option.url = 'https://ccx.upbit.com/api/v1/orders?state=wait&limit=300&page=1&order_by=desc' ;
            option.encoding = null ;
            break ;

        // arguments[1] : market Name ( KRW-BTC ... )
        case 'ORDER_CHANCE':
            option.method = 'GET' ;
            option.url = 'https://ccx.upbit.com/api/v1/members/me/order_chance?market=' + arguments[1];
            option.encoding = null ;
            break ;

        case 'ASSETS':
            option.method = 'GET' ;
            option.url = 'https://ccx.upbit.com/api/v1/investments/assets' ;
            option.encoding = null ;
            break ;

        default:
            console.log( "'getRequestOption()' Unkown Request Type!" ) ;
            break ;
    }

    return option ;
} ;

function renewToken()
{
    return new Promise( function( resolve ) {
        request( getRequestOption( 'RENEW' ) , function( error, res , body ) {
            if( !error && res.statusCode === 200 )
            {
                var encoding = res.headers[ 'content-encoding' ] ;
                if( encoding && encoding.indexOf( 'gzip' ) >= 0 )
                {
                    zlib.gunzip( body , async function( error , buffer ) {
                        if( !error )
                        {
                            /* JSON Form
                            {
                                access_token : "eyJhbGciOiJIUzI1NiJ9.eyJhcGkiOnsiYWNjZXNzX2tleSI6ImlOSDVSM0Y0enZSOThGZG1KNXVlQ0dRWWJQQWNkUjVQQ2FWWndQMHAiLCJzZWNyZXRfa2V5IjoieXFRS1BDRlc2bVlEUlBVdnF5THRYbVJRTk1CRm11dFd2NVVvWjFrUCJ9fQ.02x_8y-DQkf9dZSQKHMVN4HyriwNSq7aJEA1X8N-jXQ"
                                created_at : 1520606690
                                expires_in : 3600
                                member_uuid : "35f76a39-faf5-492f-b936-1143407b62be"
                                refresh_token : "3eb22b07e44396e058ab87cc70ff81a1bfb3799519e0aa74e0ae4e7c834bcb01"
                                scope : "all"
                                token_type : "jwt"
                            }
                            */

                            
                            var refreshData = JSON.parse( buffer.toString() ) ;

                            if( refreshData.scope === 'all' )
                            {
                                var tokenSplit = refreshData.access_token.split( '.' ) ;
                                var newToken = tokenSplit[1] ;
                                token = newToken ;
                                refreshToken = refreshData.refresh_token ;
                                renewCount++ ;
                                console.log( refreshData ) ;
                                await setAPI() ;
                                console.log( "[renewToken] API reset!" ) ;

                                resolve( true ) ;
                            }
                            else
                            {
                                console.log( refreshData ) ;
                                console.log( "'renewToken' Function Error." ) ;
                                console.log( "'renewToken' | scope is not ALL." ) ;
                                process.exit(1) ;
                            }
                        }
                        else
                        {
                            console.log( body ) ;
                            console.log( "'renewToken' Function Error." ) ;
                            console.log( "'renewToken' | zlib gunzip Failed!" ) ;
                            process.exit(1) ;
                        }
                    }) ;
                }
            }
            else
            {
                zlib.gunzip( body , ( error , buffer ) => {
                    if( !error )
                    {
                        console.log( buffer.toString() ) ;
                        console.log( "'renewToken' Function Error." ) ;
                        console.log( "'renewToken' | statusCode = " , res.statusCode ) ;
                    }
                    resolve( false ) ;
                }) ;
            }
        }) ;
    })
}

// marketName : KRW-BTC ...
function getBalance( marketName )
{
    return new Promise( function( resolve , reject ) {
        request( getRequestOption( 'ORDER_CHANCE' , marketName ) , async function( error , res , body ) {
            if( !error && res.statusCode === 200 )
            {
                var encoding = res.headers[ 'content-encoding' ] ;
                if( encoding && encoding.indexOf( 'gzip' ) >= 0 )
                {
                    zlib.gunzip( body , ( error , buffer ) => {
                        if( !error )
                        {
                            // console.log( JSON.parse( buffer.toString() ) ) ;
                            resolve( JSON.parse( buffer.toString() ) ) ;
                            /* JSON Form
                            {
                                bid_fee: '0.0005',
                                ask_fee: '0.0005',
                                market:
                                { id: 'KRW-BTC',
                                    name: 'BTC/KRW',
                                    order_types: [ 'limit' ],
                                    order_sides: [ 'ask', 'bid' ],
                                    bid: { currency: 'KRW', price_unit: null, min_total: 500 },
                                    ask: { currency: 'BTC', price_unit: null, min_total: 500 },
                                    state: 'active',
                                    krw_simple_bid: { available: false, min: 500, max: 100000 },
                                    market_order_locking_buffer_factor: '1.15' },
                                bid_account:
                                { currency: 'KRW',
                                    balance: '282791.4',
                                    locked: '17208.6',
                                    avg_krw_buy_price: '0' },
                                ask_account:
                                { currency: 'BTC',
                                    balance: '0.0',
                                    locked: '0.0',
                                    avg_krw_buy_price: '0' } }
                            }
                            */
                        }
                        else
                        {
                            console.log( body.toString() ) ;
                            console.log( "'getBalance' Function Error." ) ;
                            console.log( "'getBalance' | zlib gunzip Failed!" ) ;
                            reject() ;
                        }
                    }) ;
                }
                else
                {
                    resolve( false ) ;
                }
            }
            else if( res === undefined )
            {
                console.log( "'getBalance' Function Error." ) ;
                console.log( "잔고 확인 일시 오류" ) ;
                resolve( false ) ;
                return ;
            }
            else
            {
                console.log( body.toString() ) ;
                console.log( "'getBalance' Function Error." ) ;
                console.log( "'getBalance' | statusCode = " , res.statusCode ) ;

                var jsonBody = JSON.parse( body.toString() ) ;
                if( jsonBody.error.name === 'V1::Exceptions::ExpiredAccessKey' )
                {
                    if( loginState === 0 )
                    {
                        loginState = 1 ;
                        console.log( "재로그인이 필요합니다. (getBalance)" ) ;

                        // 재로그인 시도.
                        console.log( "재로그인 시도. (getBalance)" ) ;
                        var result = await renewToken() ;
                        if( result === true )
                        {
                            console.log( "재로그인 성공 (getBalance)" ) ;
                            loginState = 0 ;
                            resolve( false ) ;
                            return ;
                        }
                        else
                        {
                            console.log( "재로그인 실패." ) ;
                            process.exit(1) ;
                        }
                    }
                    else
                    {
                        resolve( false ) ;
                    }
                }
                else
                {
                    resolve( false ) ;
                }
            }
        }) ;
    }) ;
} ;

function getOrderList()
{
    return new Promise( function( resolve , reject ) {
        request( getRequestOption( 'ORDER_LIST' ) , async function( error , res , body ) {
            if( !error && res.statusCode === 200 )
            {
                var encoding = res.headers[ 'content-encoding' ] ;
                if( encoding && encoding.indexOf( 'gzip' ) >= 0 )
                {
                    zlib.gunzip( body , ( error , buffer ) => {
                        if( !error )
                        {
                            // console.log( JSON.parse( buffer.toString() ) ) ;
                            var orderList = [] ;
                            var orders = JSON.parse( buffer.toString() ) ;
                            for( var i in orders )
                            {
                                var orderData = { uuid: '' , time: '' } ;
                                orderData.uuid = orders[i].uuid ;
                                orderData.time = orders[i].created_at ;
                                orderData.side = orders[i].side ;
                                orderData.market = orders[i].market ;

                                orderList.push( orderData ) ;
                            }
                            
                            resolve( orderList ) ;
                            /* JSON Form
                            {
                                [{ uuid: 'f06a0e70-02fc-4719-9f82-544ea1b1d72c',
                                side: 'bid',
                                ord_type: 'limit',
                                price: '14000000.0',
                                avg_price: '0.0',
                                state: 'wait',
                                market: 'KRW-BTC',
                                created_at: '2017-12-16T14:13:42+09:00',
                                volume: '0.0001',
                                remaining_volume: '0.0001',
                                reserved_fee: '0.7',
                                remaining_fee: '0.7',
                                paid_fee: '0.0',
                                locked: '1400.7',
                                executed_volume: '0.0',
                                trades_count: 0 },
                                { uuid: 'c8a1e8f4-683a-4fb2-aaaa-87a2951b2e3a',
                                side: 'bid',
                                ord_type: 'limit',
                                price: '13000000.0',
                                avg_price: '0.0',
                                state: 'wait',
                                market: 'KRW-BTC',
                                created_at: '2017-12-16T12:03:56+09:00',
                                volume: '0.001',
                                remaining_volume: '0.001',
                                reserved_fee: '6.5',
                                remaining_fee: '6.5',
                                paid_fee: '0.0',
                                locked: '13006.5',
                                executed_volume: '0.0',
                                trades_count: 0 }]
                            }
                            */
                        }
                        else
                        {
                            console.log( "'getOrderList' Function Error." ) ;
                            console.log( "'getOrderList' | zlib gunzip Failed!" ) ;
                            reject() ;
                        }
                    }) ;
                }
                else
                {
                    // if count === 0, this area!
                    var orderList = [] ;
                    resolve( orderList ) ;
                }
            }
            else
            {
                console.log( body.toString() ) ;
                console.log( "'getOrderList' Function Error." ) ;
                console.log( "'getOrderList' | statusCode = " + res.statusCode ) ;

                var jsonBody = JSON.parse( body.toString() ) ;
                if( jsonBody.error.name === 'V1::Exceptions::ExpiredAccessKey' )
                {
                    if( loginState === 0 )
                    {
                        loginState = 1 ;
                        console.log( "재로그인이 필요합니다. (getOrderList)" ) ;

                        // 재로그인 시도.
                        console.log( "재로그인 시도. (getOrderList)" ) ;
                        var result = await renewToken() ;
                        if( result === true )
                        {
                            console.log( "재로그인 성공 (getOrderList)" ) ;
                            loginState = 0 ;
                            resolve( false ) ;
                            return ;
                        }
                        else
                        {
                            console.log( "재로그인 실패." ) ;
                            process.exit(1) ;
                        }
                    }
                    else
                    {
                        resolve( false ) ;
                        return ;
                    }
                }
                else
                {
                    resolve( false ) ;
                }
            }
        }) ;
    })
} ;

// bid : 매수 | ask : 매도
// market : KRW-BTC
function order( side , market , price , volume )
{
    return new Promise( function( resolve ) {
        var form = {
            market: market ,
            ord_type: 'limit' ,
            price: price ,
            side: side ,
            volume: volume
        } ;

        // console.log( getRequestOption( 'ORDER' , form ) ) ;
        request( getRequestOption( 'ORDER' , form ) , async function( error , res , body ) {
            var data = JSON.parse( body ) ;

            if( !error && res.statusCode === 201 )
                resolve( data.uuid ) ;
            else
            {
                console.log( form ) ;
                console.log( data.error ) ;
                console.log( "'Order' Function Error." ) ;
                console.log( "'Order' | statusCode = " + res.statusCode ) ;
                if( data.error.name === 'V1::Exceptions::ExpiredAccessKey' )
                {
                    if( loginState === 0 )
                    {
                        loginState = 1 ;
                        console.log( "재로그인이 필요합니다. (Order)" ) ;

                        // 재로그인 시도.
                        console.log( "재로그인 시도. (Order)" ) ;
                        var result = await renewToken() ;
                        if( result === true )
                        {
                            console.log( "재로그인 성공 (Order)" ) ;
                            loginState = 0 ;
                            resolve( false ) ;
                            return ;
                        }
                        else
                        {
                            console.log( "재로그인 실패." ) ;
                            process.exit(1) ;
                        }
                    }
                    else
                    {
                        resolve( false ) ;
                        return ;
                    } 
                }
                resolve( false ) ;
                return ;
            }
        }) ;
    }) ;
} ; 

function deleteOrder( uuid )
{
    return new Promise( function( resolve , reject ) {
        // console.log( getRequestOption( 'ORDER' , 'DELETE' , uuid ) ) ;
        request( getRequestOption( 'ORDER_DELETE' , uuid ) , async function( error , res , body ) {
            if( !error && res.statusCode === 200 )
            {
                resolve( "Order delete success! [uuid: " + uuid + "]" ) ;
                /* Do not use response data of order delete / before using this code, you have to check out if request encoding value is null.
                {
                    
                }
                */
            }
            else
            {
                console.log( body.toString() ) ;
                console.log( "'deleteOrder' Function Error." ) ;
                console.log( "'deleteOrder' | statusCode = " + res.statusCode ) ;

                var jsonBody = JSON.parse( body.toString() ) ;
                if( jsonBody.error.name === 'V1::Exceptions::ExpiredAccessKey' )
                {
                    if( loginState === 0 )
                    {
                        loginState = 1 ;
                        console.log( "재로그인이 필요합니다. (deleteOrder)" ) ;

                        // 재로그인 시도.
                        console.log( "재로그인 시도. (deleteOrder)" ) ;
                        var result = await renewToken() ;
                        if( result === true )
                        {
                            console.log( "재로그인 성공 (deleteOrder)" ) ;
                            loginState = 0 ;
                            resolve( false ) ;
                            return ;
                        }
                        else
                        {
                            console.log( "재로그인 실패." ) ;
                            process.exit(1) ;
                        }
                    }
                    else
                    {
                        resolve( false ) ;
                        return ;
                    } 
                }

                resolve( false ) ;
                return ;
            }
        }) ;
    }) ;
} ;

async function deleteOrderAll( uuidList )
{
    var orderList = await getOrderList() ;   
    return new Promise( function( resolve , reject ) {
        for( var i = 0 ; i < orderList.length ; ++i )
        {
            console.log( uuid ) ;
            deleteOrder( uuid ) ;
        }
        resolve( "All order delete success! [count: " + orderList.length + ']' ) ;        

        // interval = setInterval( async function() {
        //     if( uuidList.length === i ) 
        //     {
        //         clearInterval( interval ) ;
        //         resolve( "All order delete success! [count: " + i + ']' ) ;
        //     }   
        //     else
        //     {
        //         await deleteOrder( uuidList[i++] ) ;
        //     }
        // } , 50 ) ;
        
    }) ;
} ;

function getAsset( coinName )
{
    return new Promise( function( resolve , reject ) {
        request( getRequestOption( 'ASSETS' ) , async function( error , res , body ) {
            if( !error && res.statusCode === 200 )
            {
                var encoding = res.headers[ 'content-encoding' ] ;
                if( encoding && encoding.indexOf( 'gzip' ) >= 0 )
                {
                    zlib.gunzip( body , ( error , buffer ) => {
                        if( !error )
                        {
                            var assets = JSON.parse( buffer.toString() ) ;
                            var targetAsset = { balance: 0 , locked: 0 , avgPrice: 0 } ;

                            
                            assets.forEach( function( asset ) {
                                if( asset.currency === coinName )
                                {
                                    targetAsset.balance = parseFloat( asset.balance ) ;
                                    targetAsset.locked = parseFloat( asset.locked ) ;
                                    targetAsset.avgPrice = parseFloat( asset.avg_krw_buy_price ) ;
                                }
                            }) ;
                            resolve( targetAsset ) ;
                            
                            /* JSON Form
                            {
                                [ { currency: 'KRW',
                                    balance: '1500588.56538446',
                                    locked: '0.0',
                                    avg_krw_buy_price: '0' },
                                { currency: 'BTC',
                                    balance: '0.00000041',
                                    locked: '0.0',
                                    avg_krw_buy_price: '21539000' } ]
                            }
                            */
                        }
                        else
                        {
                            console.log( "'getAsset' Function Error." ) ;
                            console.log( "'getAsset' | zlib gunzip Failed!" ) ;
                            reject() ;
                        }
                    }) ;
                }
            }
            else
            {
                console.log( body.toString() ) ;
                console.log( "'getAsset' Function Error." ) ;
                console.log( "'getAsset' | statusCode = " + res.statusCode ) ;

                var jsonBody = JSON.parse( body.toString() ) ;
                if( jsonBody.error.name === 'V1::Exceptions::ExpiredAccessKey' )
                {
                    if( loginState === 0 )
                    {
                        loginState = 1 ;
                        console.log( "재로그인이 필요합니다. (getAsset)" ) ;

                        // 재로그인 시도.
                        console.log( "재로그인 시도. (getAsset)" ) ;
                        var result = await renewToken() ;
                        if( result === true )
                        {
                            console.log( "재로그인 성공 (getAsset)" ) ;
                            loginState = 0 ;
                            resolve( false ) ;
                            return ;
                        }
                        else
                        {
                            console.log( "재로그인 실패." ) ;
                            process.exit(1) ;
                        }
                    }
                    else
                    {
                        resolve( false ) ;
                        return ;
                    } 
                }
                else
                {
                    resolve( false ) ;
                    return ;
                }
            }
        }) ;
    }) ;
}

function getAssetAll()
{
    return new Promise( function( resolve , reject ) {
        request( getRequestOption( 'ASSETS' ) , async function( error , res , body ) {

            if( !error && res.statusCode === 200 )
            {
                var encoding = res.headers[ 'content-encoding' ] ;
                if( encoding && encoding.indexOf( 'gzip' ) >= 0 )
                {
                    zlib.gunzip( body , ( error , buffer ) => {
                        if( !error )
                        {
                            var assets = JSON.parse( buffer.toString() ) ;
                            resolve( assets ) ;
                            /* JSON Form
                            {
                                [ { currency: 'KRW',
                                    balance: '1500588.56538446',
                                    locked: '0.0',
                                    avg_krw_buy_price: '0' },
                                { currency: 'BTC',
                                    balance: '0.00000041',
                                    locked: '0.0',
                                    avg_krw_buy_price: '21539000' } ]
                            }
                            */
                        }
                        else
                        {
                            console.log( "'getAssetAll' Function Error." ) ;
                            console.log( "'getAssetAll' | zlib gunzip Failed!" ) ;
                            reject() ;
                        }
                    }) ;
                }
                else
                {
                    resolve( false ) ;
                }
            }
            else
            {
                console.log( body.toString() ) ;
                console.log( "'getAssetAll' Function Error." ) ;
                console.log( "'getAssetAll' | statusCode = " , res.statusCode ) ;

                var jsonBody = JSON.parse( body.toString() ) ;
                if( jsonBody.error.name === 'V1::Exceptions::ExpiredAccessKey' )
                {
                    if( loginState === 0 )
                    {
                        loginState = 1 ;
                        console.log( "재로그인이 필요합니다. (getAssetAll)" ) ;

                        // 재로그인 시도.
                        console.log( "재로그인 시도. (getAssetAll)" ) ;
                        var result = await renewToken() ;
                        if( result === true )
                        {
                            console.log( "재로그인 성공 (getAssetAll)" ) ;
                            loginState = 0 ;
                            resolve( false ) ;
                            return ;
                        }
                        else
                        {
                            console.log( "재로그인 실패." ) ;
                            process.exit(1) ;
                        }
                    }
                    else
                    {
                        resolve( false ) ;
                        return ;
                    } 
                }
                else
                {
                    resolve( false ) ;
                    return ;
                }
            }
        }) ;
    }) ;
}

function getNotContractCheck()
{
    return new Promise( function( resolve , reject ) {
        request( getRequestOption( 'ASSETS' ) , function( error , res , body ) {
            if( !error && res.statusCode === 200 )
            {
                var encoding = res.headers[ 'content-encoding' ] ;
                if( encoding && encoding.indexOf( 'gzip' ) >= 0 )
                {
                    zlib.gunzip( body , ( error , buffer ) => {
                        if( !error )
                        {
                            var assets = JSON.parse( buffer.toString() ) ;
                            var coinName = false ;
                                
                            assets.forEach( function( asset ) {
                                if( ( asset.currency !== 'KRW' ) &&
                                    ( asset.locked !== '0.0' ) )
                                {
                                    coinName = asset.currency ;
                                }
                            }) ;
                            resolve( coinName ) ;
                            /* JSON Form
                            {
                                [ { currency: 'KRW',
                                    balance: '1500588.56538446',
                                    locked: '0.0',
                                    avg_krw_buy_price: '0' },
                                { currency: 'BTC',
                                    balance: '0.00000041',
                                    locked: '0.0',
                                    avg_krw_buy_price: '21539000' } ]
                            }
                            */
                        }
                        else
                        {
                            console.log( "'getNotContractCheck' Function Error." ) ;
                            console.log( "'getNotContractCheck' | zlib gunzip Failed!" ) ;
                            reject() ;
                        }
                    }) ;
                }
            }
            else
            {
                console.log( res ) ;
                console.log( "'getNotContractCheck' Function Error." ) ;
                console.log( "'getNotContractCheck' | statusCode = " + res.statusCode ) ;
                reject() ;
            }
        }) ;
    }) ;
}


// function getNominalPrice( price )
// {
//     if( price >= 2000000 )
//         return 1000 ;
//     else if( ( 2000000 > price ) && ( price >= 1000000 ) )
//         return 500 ;
//     else if( ( 1000000 > price ) && ( price >= 500000 ) )
//         return 100 ;
//     else if( ( 500000 > price ) && ( price >= 100000 ) )
//         return 50 ;
//     else if( ( 100000 > price ) && ( price >= 10000 ) )
//         return 10 ;
//     else if( ( 10000 > price ) && ( price >= 1000 ) )
//         return 1 ;
//     else   
//         return 0 ;
// }


function ConnectWebSocket( coinNameList , callback )
{
    if( coinNameList === undefined )
    {
        console.log( "ConnectWebSocket() | Parameter Error. ( you must add coin list. ) " ) ;
        return ;
    }

    var codes = ['\"CRIX.UPBIT.KRW-BTC\"'] ;
    for( var i = 0 ; i < coinNameList.length; ++i )
    {
        codes.push( '\"CRIX.UPBIT.KRW-' + coinNameList[i] + '\"' ) ;
        codes.push( '\"CRIX.UPBIT.BTC-' + coinNameList[i] + '\"' ) ;
    }
    var joinedCodes = codes.join( ',' ) ;

    var form = "[{\"ticket\":\"ram macbook\"},{\"type\":\"crixOrderbook\",\"codes\":[" + joinedCodes + "]}]" ;

    // console.log( form ) ;

    const socket = new ws( 'wss://crix-websocket.upbit.com/sockjs/147/31gyxe0q/websocket' , {
        origin: 'https://upbit.com' ,
        host: 'crix-websocket.upbit.com'
    } ) ;

    
    socket.on( 'open' , function() {
        console.log( 'ConnectWebSocket() | Socket Connection Success!' ) ;
        socket.send( JSON.stringify( form ) , function( error ) {
            if( error )
            {
                console.log( 'ConnectWebSocket() | socket.send() Error!' ) ;
                callback( false ) ;
            }
            else
            {
                callback( true ) ;
            }
        }) ;
    }) ;

    socket.on( 'message' , function( data ) {
        if( ( data !== 'h' ) && ( data !== 'o' ) )
        {
            try {
                var data = JSON.parse( data.replace( 'a["{' , '["{' ) ) ;
                var refinedData = JSON.parse( data[0] ) ;
            } catch( error ) {
                console.log( 'ConnectWebSocket() | Socket Message | Invalid JSON Data.' ) ;
            }
            if( refinedData.type === 'crixOrderbook' )
            {    
                // console.log( refinedData.code.slice( 11 ) , ": " , refinedData.timestamp ) ;
                coinPriceMap.set( refinedData.code.slice( 11 ) , // 'CRIX.UPBIT.BTC-LTC' -> 'BTC-LTC',
                                  { market: refinedData.code.slice( 11 ) ,
                                    orderbookUnits: refinedData.orderbookUnits ,
                                    timestamp: refinedData.timestamp }) ;
            }
        }
    }) ;

    socket.on( 'error' , function( error ) {
        console.error( 'ConnectWebSocket() | WebSocket Error ' + error ) ;
        process.exit(1) ;
    }) ;
}

function checkCoinPriceMap( targetCoin , callback )
{
    var interval = setInterval( function() {
        if( coinPriceMap.size === ( ( targetCoin.length * 2 ) + 1 ) )
        {
            callback() ;
            clearInterval( interval ) ;
        }
    } , 100 ) ;
}

async function setCoinProfitMap( targetCoin )
{
    var interval = setInterval( function() {
        for( var i in targetCoin )
        {
            calcProfit( targetCoin[i] ) ;
        }
    } , 50 ) ;
}


function calcProfit( coinName )
{
    var BTCPrice = coinPriceMap.get( 'KRW-BTC' ) ;
    var KRWofCoin = coinPriceMap.get( 'KRW-' + coinName ) ;
    var BTCofCoin = coinPriceMap.get( 'BTC-' + coinName ) ;

    // 호가는 즉시 구매 가능한 coin의 가격을 기준으로 계산한다. ( 최저 매도가격 )
    // var nominalPrice = getNominalPrice( KRWofCoin.askPrice ) ;

    // 즉시 판매 가능한 BTC 가격 -> 최고 매수가격.
    var sellBTC = BTCPrice.orderbookUnits[0].bidPrice ;
    var sellBTCsize = BTCPrice.orderbookUnits[0].bidSize ;
    // 즉시 구매 가능한 BTC 가격 -> 최저 매도가격.
    var buyBTC = BTCPrice.orderbookUnits[0].askPrice ;
    var buyBTCsize = BTCPrice.orderbookUnits[0].askSize ;


    var slowest = KRWofCoin.timestamp > BTCofCoin.timestamp ? KRWofCoin.timestamp : BTCofCoin.timestamp ;    
    var margin = 0.002 ;

    // Phase 1
    var buySizeCheck = 0 ;
    var coinSizeOfPhase1 = 0 ;
    var buyPriceOfPhase1 = 0 ;
    var sellPriceOfPhase1 = 0 ;
    var needOfPhase1 = 0 ;
    var min_j = 0 ;
    var downsizedOfPhase1 = false ;

    for( var i = 0 ; i < 9 ; ++i )
    {
        buySizeCheck += KRWofCoin.orderbookUnits[i].askSize ;
        var criteria = KRWofCoin.orderbookUnits[i].askPrice / sellBTC * ( 1 + g_KRW_fee + g_BTC_fee + margin ) ;

        // 첫 즉판가에 팔아도 이익이 안나면 continue
        if( criteria > BTCofCoin.orderbookUnits[0].bidPrice )
            continue ;

        var sellSizeCheck = 0 ;
        for( var j = 0 ; j < 9 ; ++j )
        {
            // 팔아도 이익이 나는 구간이면 개수 포함.
            if( BTCofCoin.orderbookUnits[j].bidPrice >= criteria )
            {
                sellSizeCheck += BTCofCoin.orderbookUnits[j].bidSize ;
                min_j = min_j <= j ? min_j : j ;
            }
            else
                break ;
        }

        var xu = buySizeCheck < sellSizeCheck ? buySizeCheck : sellSizeCheck ;
        if( xu > coinSizeOfPhase1 )
        {
            coinSizeOfPhase1 = xu ;
            buyPriceOfPhase1 = KRWofCoin.orderbookUnits[i].askPrice ;
            sellPriceOfPhase1 = BTCofCoin.orderbookUnits[min_j].bidPrice
            needOfPhase1 = coinSizeOfPhase1 * buyPriceOfPhase1 * ( 1 + g_KRW_fee ) ;
        }
    }


    if( currentBalance.KRW.balance < needOfPhase1 )
    {
        // console.log( "[phase1] 현재 금액보다 요구액이 더 큽니다. [" , currentBalance.KRW.balance , " | " , needOfPhase1 , "]" ) ;
        // 현재 금액의 70%만 구매할 것.
        if( currentBalance.KRW.balance > 0 )
        {
            coinSizeOfPhase1 = ( currentBalance.KRW.balance * 0.7 ) / ( buyPriceOfPhase1 * (1+g_KRW_fee) ) ;
            needOfPhase1 = currentBalance.KRW.balance * 0.7 ;
            downsizedOfPhase1 = true ;
        }
        else
        {
            console.log( "현재 현금보유액이 0원 입니다. " ) ;
            process.exit(1) ;
        }
    }
    
    if( coinSizeOfPhase1 > 0 &&  needOfPhase1 > 50000 )
    {
        // console.log( "----------------------------" ) ;
        var profit = (sellPriceOfPhase1 * coinSizeOfPhase1 * sellBTC * (1-g_BTC_fee)) - ( buyPriceOfPhase1 * coinSizeOfPhase1 * (1+g_KRW_fee) ) ;
        // console.log( coinName , "[phase1] |" , profit , "|" , buyPriceOfPhase1 , "|" , sellPriceOfPhase1 , "|" , coinSizeOfPhase1 , "|" , sellBTC ) ;         
        // for( var i in KRWofCoin.orderbookUnits )
        //     console.log( "즉구가 " , KRWofCoin.orderbookUnits[i].askPrice , KRWofCoin.orderbookUnits[i].askSize ) ;
        // for( var i in BTCofCoin.orderbookUnits )
        //     console.log( "즉판가 " , BTCofCoin.orderbookUnits[i].bidPrice , BTCofCoin.orderbookUnits[i].bidSize ) ;  
        coinProfitMap_phase1.set( coinName , {
            coinName: coinName ,
            phase: 1 ,
            need: needOfPhase1 ,
            profit: profit ,
            downsized: downsizedOfPhase1 ,
            timestamp: slowest ,
            first: { side: 'bid' , market: 'KRW-' + coinName , price: buyPriceOfPhase1 , size: coinSizeOfPhase1 } ,
            second: { side: 'ask' , market: 'BTC-' + coinName , price: sellPriceOfPhase1 , size: coinSizeOfPhase1 }
        }) ;
        // console.log( "----------------------------" ) ;
    }
    else
    {
        coinProfitMap_phase1.delete( coinName ) ;
    }


    // Phase2
    buySizeCheck = 0 ;
    var coinSizeOfPhase2 = 0 ;
    var buyPriceOfPhase2 = 0 ;
    var sellPriceOfPhase2 = 0 ;
    var needOfPhase2 = 0 ;
    min_j = 0 ;
    var downsizedOfPhase2 = false ;

    for( var i = 0 ; i < 9 ; ++i )
    {
        buySizeCheck += BTCofCoin.orderbookUnits[i].askSize ; // 113.58800454 + 596.57199546
        var criteria = BTCofCoin.orderbookUnits[i].askPrice * buyBTC * ( 1 + g_KRW_fee + g_BTC_fee + margin ) ; 
        // 0.00076769 * 12435000 * (1+0.0025+0.0005+0.002) 9593.956275749999
        // 0.00077 * 12435000 * (1+0.0025+0.0005+0.002) 9622.824749999998

        // 첫 즉판가에 팔아도 이익이 안나면 continue
        if( criteria > KRWofCoin.orderbookUnits[0].bidPrice )
            continue ;

        var sellSizeCheck = 0 ;
        for( var j = 0 ; j < 9 ; ++j )
        {
            // 팔아도 이익이 나는 구간이면 개수 포함.
            if( KRWofCoin.orderbookUnits[j].bidPrice >= criteria )
            {
                sellSizeCheck += KRWofCoin.orderbookUnits[j].bidSize ; // 215.27585688 + 2167.03951869
                min_j = min_j <= j ? min_j : j ;
            }
            else
                break ;
        }

        var xu = buySizeCheck > sellSizeCheck ? sellSizeCheck : buySizeCheck ; // 710.1600000000001
        if( xu > coinSizeOfPhase2 )
        {
            coinSizeOfPhase2 = xu ; 
            buyPriceOfPhase2 = BTCofCoin.orderbookUnits[i].askPrice ;
            sellPriceOfPhase2 = KRWofCoin.orderbookUnits[min_j].bidPrice ; 
            needOfPhase2 = coinSizeOfPhase2 * buyPriceOfPhase2 * ( 1 + g_BTC_fee ) ;
        }
    }

    if( currentBalance.BTC.balance < needOfPhase2 )
    {
        // console.log( "[phase1] 현재 금액보다 요구액이 더 큽니다. [" , currentBalance.KRW.balance , " | " , needOfPhase1 , "]" ) ;
        // 현재 금액의 70%만 구매할 것.
        if( currentBalance.BTC.balance > 0 )
        {
            coinSizeOfPhase2 = ( currentBalance.BTC.balance * 0.7 ) / ( buyPriceOfPhase2 * (1+g_BTC_fee) ) ;
            needOfPhase2 = currentBalance.BTC.balance * 0.7 ;
            downsizedOfPhase2 = true ;
        }
    }


    if( coinSizeOfPhase2 > 0 &&  needOfPhase2 > 0.0005 )
    {
        // console.log( "----------------------------" ) ;
        var profit = (sellPriceOfPhase2 * coinSizeOfPhase2 * (1-g_KRW_fee)) - ( buyPriceOfPhase2 * coinSizeOfPhase2 * buyBTC * (1+g_BTC_fee) ) ;
        // if( currentBalance.BTC.avgPrice > buyBTC )
        //     console.log( coinName , "[phase2] |" , profit , "|" , buyPriceOfPhase2 , "|" , sellPriceOfPhase2 , "|" , coinSizeOfPhase2 , "|" , currentBalance.BTC.avgPrice , "(BTC구매가보다 평단가가 더 높음)" ) ;
        // else
        //     console.log( coinName , "[phase2] |" , profit , "|" , buyPriceOfPhase2 , "|" , sellPriceOfPhase2 , "|" , coinSizeOfPhase2 , " | " , buyBTC ) ;
        // for( var i in KRWofCoin.orderbookUnits )
        //     console.log( "즉구가 " , BTCofCoin.orderbookUnits[i].askPrice , BTCofCoin.orderbookUnits[i].askSize ) ;
        // for( var i in BTCofCoin.orderbookUnits )
        //     console.log( "즉판가 " , KRWofCoin.orderbookUnits[i].bidPrice , KRWofCoin.orderbookUnits[i].bidSize ) ;

        coinProfitMap_phase2.set( coinName , {
            coinName: coinName ,
            phase: 2 ,
            need: needOfPhase2 ,
            profit: profit ,
            downsized: downsizedOfPhase2 ,
            timestamp: slowest ,
            first: { side: 'bid' , market: 'BTC-' + coinName , price: buyPriceOfPhase2 , size: coinSizeOfPhase2 } ,
            second: { side: 'ask' , market: 'KRW-' + coinName , price: sellPriceOfPhase2 , size: coinSizeOfPhase2 }
        }) ;
        // console.log( "----------------------------" ) ;
    }
    else
    {
        coinProfitMap_phase2.delete( coinName ) ;
    }
}


function showProfitMap( targetCoin ) {
    setInterval( function() {
        var length = targetCoin.length ;
        for( var i = 0 ; i < length ; ++i )
        {
            var phase1 = coinProfitMap_phase1.get( targetCoin[i] ) ;
            var phase2 = coinProfitMap_phase2.get( targetCoin[i] ) ;

            if( phase1 !== undefined )
            {
                console.log( targetCoin[i] , ' Phase1' ) ;
                console.log( phase1 ) ;
                console.log("\n") ;
            }
            if( phase2 !== undefined )
            {
                console.log( targetCoin[i] , ' Phase2' ) ;
                console.log( phase2 ) ;
                console.log("\n") ;
            }
        }
    }, 100 ) ;
}


function getMostProfitCase( callback )
{
    var profitList = new Array() ; // phase1 , phase2 를 섞은 배열 생성 -> 수익률대로 정렬
    var minProfit = 500 ;

    coinProfitMap_phase1.forEach( (value , key) => {
        if( value.profit > minProfit )
                profitList.push( value ) ;
    });

    coinProfitMap_phase2.forEach( (value , key) => {
        if( value.profit > minProfit )
                profitList.push( value ) ;
    });

    // Profit 순으로 정렬.
    profitList.sort( compareProfit ) ;
    
    if( profitList.length > g_priority )
        callback( profitList[g_priority] ) ;
    else
        callback( false ) ;
}

// 음수반환 : a 가 먼저옴.
function compareProfit( a , b ) {
    return b.profit - a.profit ;
}

function setCurrentBalance()
{
    return new Promise( async function( resolve ) {
        var data = await getBalance( 'KRW-BTC' ) ;
        if( data !== false )
        {
            var BTCPrice = coinPriceMap.get( 'KRW-BTC' ) ;

            currentBalance.KRW.balance = parseFloat( data.bid_account.balance ) ;
            currentBalance.KRW.locked = parseFloat( data.bid_account.locked ) ;

            currentBalance.BTC.balance = parseFloat( data.ask_account.balance ) ;
            currentBalance.BTC.locked = parseFloat( data.ask_account.locked ) ;
            // 현재 BTC 의 avgPrice가 맛이갔음. 18.03.05 오후 7:00
            //currentBalance.BTC.avgPrice = parseFloat( data.ask_account.avg_krw_buy_price ) ;
            currentBalance.BTC.avgPrice = BTCPrice.orderbookUnits[0].bidPrice ;
            currentBalance.BTC.buyingPrice = currentBalance.BTC.balance * currentBalance.BTC.avgPrice ;
            
            // 즉판가에 대한 현재 수익률.
            currentBalance.BTC.rate = ( ( BTCPrice.orderbookUnits[0].bidPrice / currentBalance.BTC.avgPrice ) * ( 1 - g_KRW_fee ) ) - 1 

            resolve( true ) ;
        }
        else
            resolve( false ) ;
    }) ;
}

function setCurrentAssets()
{
    return new Promise( async function( resolve ) {
        var assets = await getAssetAll() ;
        if( assets !== false )
        {
            var total = 0 ;
            var assetCount = 0 ;

            for( var i in assets )
            {
                if( assets[i].currency === 'KRW' )
                {
                    currentBalance.KRW.balance = parseFloat( assets[i].balance ) ;
                    currentBalance.KRW.locked = parseFloat( assets[i].locked ) ;
                    total += currentBalance.KRW.balance + currentBalance.KRW.locked ;
                }
                else if( assets[i].currency === 'BTC' )
                {
                    var BTCPrice = coinPriceMap.get( 'KRW-BTC' ) ;

                    currentBalance.BTC.balance = parseFloat( assets[i].balance ) ;
                    currentBalance.BTC.locked = parseFloat( assets[i].locked ) ;

                    // 현재 BTC 의 avgPrice가 맛이갔음. 18.03.05 오후 7:00
                    // currentBalance.BTC.avgPrice = parseFloat( assets[i].avg_krw_buy_price ) ;
                    currentBalance.BTC.avgPrice = BTCPrice.orderbookUnits[0].bidPrice ;
                    currentBalance.BTC.buyingPrice = currentBalance.BTC.balance * currentBalance.BTC.avgPrice ;
                    total += ( (currentBalance.BTC.balance + currentBalance.BTC.locked) * currentBalance.BTC.avgPrice ) ;
                }
                else
                {
                    var coinPrice = coinPriceMap.get( 'KRW-' + assets[i].currency ) ;
                    if( coinPrice !== undefined )
                    {
                        total += ( (parseFloat( assets[i].balance ) + parseFloat( assets[i].locked )) * coinPrice.orderbookUnits[0].bidPrice ) ;
                    }
                    assetCount++ ;
                }
            }

            currentBalance.total = total ;
            currentBalance.assetCount = assetCount ;

            resolve( true ) ;
        }
        else
        {
            resolve( false ) ;
        }
    }) ;
}

function getCurrentBalance()
{
    return new Promise( function( resolve , reject ) {
        if( currentBalance.KRW.balance <= 0 )
        {
            console.log( "현금이 0원 입니다. 프로세스 종료." ) ;
            process.exit(1) ;
        }
        else
        {
            resolve( currentBalance ) ;
        }
    }) ;
}

function CoinPriceCheck( type )
{
    return new Promise( async function( resolve ) {
        var assets = await getAssetAll() ;
        if( assets === false )
        {
            resolve( false ) ;
            return ;
        }
        /* JSON Form
        {
            [ { currency: 'KRW',
                balance: '1500588.56538446',
                locked: '0.0',
                avg_krw_buy_price: '0' },
            { currency: 'BTC',
                balance: '0.00000041',
                locked: '0.0',
                avg_krw_buy_price: '21539000' } ]
        }
        */

        var length = assets.length ;
        var BTCPrice = coinPriceMap.get( 'KRW-BTC' ) ;

        for( var i = 0 ; i < length ; ++i )
        {
            if( assets[i].currency !== 'KRW' && assets[i].currency !== 'BTC' )
            {
                var avgPrice = parseFloat( assets[i].avg_krw_buy_price ) ;
                var balance = parseFloat( assets[i].balance ) ;
                if( avgPrice * balance > 5000 && balance > 0 )
                {
                    var coinPrice_KRW = coinPriceMap.get( 'KRW-' + assets[i].currency ) ;
                    var coinPrice_BTC = coinPriceMap.get( 'BTC-' + assets[i].currency ) ;

                    // ARDR 이 문제.
                    if( coinPrice_KRW === undefined )
                    {
                        console.log( "[CoinPriceCheck]" , assets[i].currency , "의 KRW 가격정보가 없습니다. " ) ;
                        continue ;
                    }
                    if( coinPrice_BTC === undefined )
                    {
                        console.log( "[CoinPriceCheck]" , assets[i].currency , "의 BTC 가격정보가 없습니다. " ) ;
                        continue ;
                    }
                    if( BTCPrice === undefined )
                    {
                        console.log( "[CoinPriceCheck] BTC 가격정보가 없습니다. " ) ;
                        continue ;
                    }

                    // KRW rate 계산.
                    var bidSize_KRW = coinPrice_KRW.orderbookUnits[0].bidSize ;
                    var bidPrice_KRW = coinPrice_KRW.orderbookUnits[0].bidPrice ;

                    var size_KRW = bidSize_KRW < balance ? bidSize_KRW : balance ;
                    // 팔가격 / 산가격
                    var rate_KRW = ( bidPrice_KRW / avgPrice ) * ( 1 - g_KRW_fee ) ;

                    // BTC rate 계산
                    var bidSize_BTC = coinPrice_BTC.orderbookUnits[0].bidSize ;
                    var bidPrice_BTC = coinPrice_BTC.orderbookUnits[0].bidPrice ;

                    var size_BTC = bidSize_BTC < balance ? bidSize_BTC : balance ;
                    var rate_BTC = ( ( bidPrice_BTC * BTCPrice.orderbookUnits[0].bidPrice ) / avgPrice ) * ( 1 - g_BTC_fee ) ;

                    if( type === 1 )
                    {
                        if( rate_KRW > 1 && ( rate_KRW > rate_BTC ) )
                        {
                            if( bidPrice_KRW * size_KRW > 50000 )
                            {
                                console.log( "[익절] rate: " , rate_KRW ) ;
                                resolve( {
                                    coinName: assets[i].currency ,
                                    rate: rate_KRW ,
                                    phase: 3 ,
                                    side: 'ask' ,
                                    market: 'KRW-' + assets[i].currency ,
                                    price: bidPrice_KRW ,
                                    size: size_KRW
                                } ) ;   
                                return ;
                            }
                        }
                        else if( rate_BTC > 1 && ( rate_KRW < rate_BTC ) )
                        {
                            if( bidPrice_BTC * size_BTC > 0.0005 )
                            {
                                console.log( "[익절] rate: " , rate_BTC ) ;
                                resolve( {
                                    coinName: assets[i].currency ,
                                    rate: rate_BTC ,
                                    phase: 4 ,
                                    side: 'ask' ,
                                    market: 'BTC-' + assets[i].currency ,
                                    price: bidPrice_BTC ,
                                    size: size_BTC
                                } ) ;   
                                return ;
                            }
                        }
                        // 2% 이상 손실일 경우 손절.
                        else if( rate_KRW < 0.98 && ( rate_KRW > rate_BTC ) )
                        {
                            if( bidPrice_KRW * size_KRW > 50000 )
                            {
                                console.log( "[손절] rate: " , rate_KRW ) ;
                                resolve( {
                                    coinName: assets[i].currency ,
                                    rate: rate_KRW ,
                                    phase: 3 ,
                                    side: 'ask' ,
                                    market: 'KRW-' + assets[i].currency ,
                                    price: bidPrice_KRW ,
                                    size: size_KRW
                                } ) ;   
                                return ;
                            }
                        }
                        else if( rate_BTC < 0.98 && ( rate_KRW < rate_BTC ) )
                        {
                            if( bidPrice_BTC * size_BTC > 0.0005 )
                            {
                                console.log( "[손절] rate: " , rate_BTC ) ;
                                resolve( {
                                    coinName: assets[i].currency ,
                                    rate: rate_BTC ,
                                    phase: 4 ,
                                    side: 'ask' ,
                                    market: 'BTC-' + assets[i].currency ,
                                    price: bidPrice_BTC ,
                                    size: size_BTC
                                } ) ;   
                                return ;
                            }
                        }
                    }
                }
            }
        }

        resolve( false ) ;
        return ;
    }) ;
}

function changeRatio( difRatio ) {
    return new Promise( async function( resolve ) {
        var BTCPrice = coinPriceMap.get( 'KRW-BTC' ) ;
        var totalBTCKRW = currentBalance.KRW.balance + currentBalance.BTC.buyingPrice ;
        var amountKRW = totalBTCKRW * Math.abs( difRatio ) ;

        if( difRatio > 0 )
        {
            // 전체 금액에서 difRatio % 만큼 
            // BTC 를 팔야아 함.
            resolve( {
                coinName: 'BTC -> KRW' ,
                rate: 0 ,
                phase: 5 ,
                side: 'ask' ,
                market: 'KRW-BTC' ,
                price: BTCPrice.orderbookUnits[5].bidPrice ,
                size: amountKRW / BTCPrice.orderbookUnits[0].bidPrice
            } ) ;   
            return ;
        }
        else if( difRatio < 0 )
        {
            // BTC 사야함.
            resolve( {
                coinName: 'KRW -> BTC' ,
                rate: 0 ,
                phase: 5 ,
                side: 'bid' ,
                market: 'KRW-BTC' ,
                price: BTCPrice.orderbookUnits[5].askPrice ,
                size: amountKRW / BTCPrice.orderbookUnits[0].askPrice
            } ) ;   
            return ;
        }
        else
        {
            resolve( false ) ;
            return ;
        }

    }) ;
}

module.exports = {
    setAPI: setAPI ,
    renewToken: renewToken ,
    getRenewCount: getRenewCount ,
    getCurrentToken: getCurrentToken ,

    getRequestOption: getRequestOption ,

    getBalance: getBalance ,

    getOrderList: getOrderList ,

    getAsset: getAsset ,

    getAssetAll: getAssetAll ,

    getNotContractCheck: getNotContractCheck ,

    order: order ,

    deleteOrder: deleteOrder ,

    deleteOrderAll: deleteOrderAll ,

    ConnectWebSocket: ConnectWebSocket ,
    checkCoinPriceMap: checkCoinPriceMap ,
    setCoinProfitMap: setCoinProfitMap ,

    showProfitMap: showProfitMap ,

    getMostProfitCase: getMostProfitCase ,

    setCurrentBalance: setCurrentBalance ,
    getCurrentBalance: getCurrentBalance ,

    setCurrentAssets: setCurrentAssets ,

    CoinPriceCheck: CoinPriceCheck ,
    changeRatio: changeRatio

} ;




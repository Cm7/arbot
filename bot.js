const upbitAPI = require( './upbitAPI.js' ) ;
const mClient = require( './client.js' ) ;
const colors = require( 'colors' ) ;
const winston = require( 'winston' ) ;
const moment = require( 'moment' ) ;
require( 'winston-daily-rotate-file' )
const fs = require( 'fs' ) ;

const logDir = 'logs' ;

// Log Directory 생성.
if( false === fs.existsSync(logDir) )
{
    fs.mkdirSync( logDir ) ;
}

// Log 설정.
var logger = new (winston.Logger) ({
    transports: [
        new (winston.transports.DailyRotateFile) ({
            level: 'info' ,
            filename: logDir + '/%DATE%-Arbot.log' ,
            timestamp: () => { return moment().format( "YYYY-MM-DD HH:mm:ss.SSS" ) } ,
            datePattern: 'YYYY-MM-DD' ,
            maxSize: '100m' ,
            pretend: true
        })
    ]
}) ;

var totalBalance = 0 ;

var orderData = {
    coinName: '' ,
    phase: 0 ,
    need: 0 ,
    profit: 0 ,
    downsized: false ,
    timestamp: 0 ,
    first: { side: '' , market: '' , price: 0 , size: 0 } ,
    second: { side: '' , market: '' , price: 0 , size: 0 }
}

var prevOrderData = {
    coinName: '' ,
    phase: 0 ,
    need: 0 ,
    profit: 0 ,
    downsized: false ,
    timestamp: 0 ,
    first: { side: '' , market: '' , price: 0 , size: 0 } ,
    second: { side: '' , market: '' , price: 0 , size: 0 }
}

var orderState = 0 ;
// 0: empty | 1: processing | 2: wait


const g_KRW_fee = 0.00051 ;
const g_BTC_fee = 0.00251 ;
const orderDelay = 500 ;

var renewCount = 0 ;
var BTCRatio = 0.5 ;

function pushOrder( data , callback )
{
    if( 0 === orderState )
    {
        // downsized 된게 아니고, 이전꺼랑 주문 정보가 똑같으면 스킵.
        if( ( prevOrderData.downsized === false ) &&
            ( prevOrderData.coinName === data.coinName ) &&
            ( prevOrderData.phase === data.phase ) &&
            ( prevOrderData.profit === data.profit ) )
        {
            callback( false ) ;
            return ;
        }
        else
        {  
            orderState = 1 ;
            prevOrderData = data ;
            orderData = data ;
            callback( true ) ;
            return ;
        }
    }
    else
    {
        callback( false ) ;
        return ;
    }
}

function goOrder( callback )
{
    orderState = 2 ;
    if( orderData.phase === 1 )
        console.log( colors.blue( orderData.phase + " | " + orderData.coinName + " | " + orderData.profit + " | " + orderData.need + " | " + orderData.first.price + " -> " + orderData.second.price ) ) ;
    else if( orderData.phase === 2 )
        console.log( colors.red( orderData.phase + " | " + orderData.coinName + " | " + orderData.profit + " | " + orderData.need + " | " + orderData.first.price + " -> " + orderData.second.price ) ) ;

    console.log( "주문 시작" ) ;
    goFirstOrder( async function( first_uuid , skipFlag ) {
        goSecondOrder( first_uuid , skipFlag , function( second_uuid , skipFlag) {


            logger.info( totalBalance + " | " + orderData.phase + " | " + orderData.coinName + " | " + orderData.profit + " | " + orderData.need + " | " + orderData.first.price + " -> " + orderData.second.price ) ;

            // 익절 모드 나머지 주문.
            goRemainedOrder( 1 , function( result ) {
                // 잉여코인 탐색 시 수익이 없음. 다음 주문을 위해 1.4초 기다림.
                console.log( "잉여 코인 판매 / BTC 비중 변경 완료." ) 
                if( result !== 0 )
                {
                    setTimeout( function() {
                        orderState = 0 ;
                        callback() ;
                    } , orderDelay ) ;
                }
                else
                {
                    // goRemainedOrder 에서 1.4초를 기다렸기 때문에 바로 콜백.
                    orderState = 0 ;
                    callback() ;
                }
            }) ;
        }) ;
    }) ;
}

async function goFirstOrder( callback )
{
    console.log( "1차 주문 시작." ) ;

    var skipFlag = false ;
    var first_uuid = await upbitAPI.order( orderData.first.side , orderData.first.market , orderData.first.price , orderData.first.size ) ;
    
    // 주문 실패 시.
    if( false === first_uuid )
    {   
        console.log( "1차 주문 실패. [이후 모든 주문 스킵]" ) ;
        skipFlag = true ;
        callback( false , skipFlag ) ;
        return ;
    }
    // 주문 성공 시 2차 주문으로 넘어감.
    else
    {
        console.log( "1차 주문 성공. [", first_uuid , "]" ) ;
        callback( first_uuid , skipFlag ) ;
        return ;
    }
}

function goSecondOrder( first_uuid , skipFlag , callback )
{
    if( skipFlag === false )
    {
        setTimeout( async function() {

            await upbitAPI.deleteOrder( first_uuid ) ;

            var second_uuid = 0 ;
            // 1차 주문의 결과 확인
            // 1. 주문 자체가 실패한 경우 이 단계를 건너뜀.
            // 2. 주문은 성공했으나 체결량이 계산과 다를 경우를 고려해야함.
            // 우선 첫 주문은 무조건 취소.
            console.log( "1차 주문 체크" ) ;
            var asset = await upbitAPI.getAsset( orderData.coinName ) ;

            if( asset === false )
            {
                console.log( "getAsset : false, 다음 주문 skip." ) ;
                skipFlag = true ;
                callback( false , skipFlag ) ;
                return ;
            }

            // 주문한 코인 양이 작을 경우.
            if( asset.balance < orderData.second.size )
            {
                orderData.second.size = asset.balance ;

                if( orderData.phase === 1 )
                {
                    if( asset.balance * orderData.second.price < 0.0005 )
                    {
                        console.log( "체결된 양이 최소 거래주문량보다 작으므로 주문 Skip." ) ;
                        console.log( "체결량: " , asset.balance , " [" , asset.balance * orderData.second.price , "]" ) ;
                        skipFlag = true ;
                        callback( false , skipFlag ) ;
                        return ;
                    }
                }
                else if( orderData.phase === 2 )
                {
                    if( ( asset.balance * orderData.second.price ) < 50000 )
                    {
                        console.log( "체결된 양이 최소 거래주문량보다 작으므로 주문 Skip." ) ;
                        console.log( "체결량: " , asset.balance , " [" , asset.balance * orderData.second.price , "]" ) ;
                        skipFlag = true ;
                        callback( false , skipFlag ) ;
                        return ;
                    }
                }
            }
            // 주문한 코인의 양이 다음 주문할 양보다 많을 경우? ( 거래체결 속도가 느려 쌓인 물량 때문 )
            else if( asset.balance > orderData.second.size )
            {
                console.log( "이미 코인이 일부 있습니다." ) ;
                if( orderData.phase === 1 )
                {
                    // 매수평균가가 샀던 금액보다 작으면 같이 판다.
                    if( ( asset.avgPrice * (1 + g_KRW_fee ) ) <= orderData.first.price )
                    {
                        console.log( "평단가 확인 후 함께 판매. ( avg: " + asset.avgPrice + " | " + orderData.first.price + " )" ) ;
                        orderData.second.size = asset.balance ;
                    }
                }
                else if( orderData.phase === 2 )
                {
                    if( ( asset.avgPrice * (1 + g_KRW_fee ) ) <= orderData.second.price )
                    {
                        console.log( "평단가 확인 후 함께 판매. ( avg: " + asset.avgPrice + " | " + orderData.second.price + " )" ) ;
                        orderData.second.size = asset.balance ;
                    }
                }
            }

            console.log( "2차 주문 시작" ) ;
            second_uuid = await upbitAPI.order( orderData.second.side , orderData.second.market , orderData.second.price , orderData.second.size ) ;

            // 주문 실패 시.
            if( false === second_uuid )
            {
                console.log( "2차 주문 실패. [이후 다음 주문 Skip]" ) ;
                skipFlag = true ;
                callback( false , skipFlag ) ;
                return ;
            }
            else
            {
                console.log( "2차 주문 성공. [" , second_uuid , "]" ) ;
                callback( second_uuid , skipFlag ) ;
                return ;
            }
        } , orderDelay ) ;
    }
    else
    {
        callback( first_uuid , skipFlag ) ;
        return ;
    }
}

// type 1 : 무조건 익절, type 2 : 손절도 침.
// result 1 : 판매완료 , 0 : 잉여코인 수익없음 , -1 : 판매 오더 실패.
function goRemainedOrder( type , callback )
{
    setTimeout( async function() {
        var profitOrder = await upbitAPI.CoinPriceCheck( type ) ;
    
        if( profitOrder !== false )
        {
            if( profitOrder.rate > 1 )
                console.log( colors.green( profitOrder.phase + " | " + profitOrder.coinName + " | " + profitOrder.rate + " | " + profitOrder.price + " | " + profitOrder.size ) ) ;
            else
                console.log( colors.grey( profitOrder.phase + " | " + profitOrder.coinName + " | " + profitOrder.rate + " | " + profitOrder.price + " | " + profitOrder.size ) ) ;
            console.log( "잉여 코인 판매 시작" ) ;
            var uuid = await upbitAPI.order( profitOrder.side , profitOrder.market , profitOrder.price , profitOrder.size ) ;

            logger.info( totalBalance + " | " + profitOrder.phase + " | " + profitOrder.coinName + " | " + profitOrder.rate + " | " + profitOrder.price + " | " + profitOrder.size ) ;

            if( uuid !== false )
            {
                // 잉여 코인 판매시, 주문과 동시에 삭제 안시킴.
                // await upbitAPI.deleteOrder( uuid )
                callback( 1 ) ;
                return ;
            }
            else
            {
                console.log( "잉여 코인 판매 에러." ) ;
                callback( -1 ) ;
                return ;
            }
        }
        else
        {
            console.log( "잉여코인 수익 없음." ) ;
            console.log( "BTC 보유 비율 조절 (BTC ratio : " + BTCRatio + ")" ) ;
            
            // 비코 원화 보유 밸런스 맞추기.
            // BTC 보유 비율 구하기.
            if( await upbitAPI.setCurrentBalance() === true )
            {
                var currentBalance = await upbitAPI.getCurrentBalance() ;
                var totalBTCKRW = currentBalance.BTC.buyingPrice + currentBalance.KRW.balance ;
                var currentBTCRatio = Math.floor( (currentBalance.BTC.buyingPrice / totalBTCKRW)*100 ) / 100 ;
                var difRatio = Math.floor( (currentBTCRatio - BTCRatio)*1000 ) / 1000 ;
                
                console.log( "원화: " + Math.floor(currentBalance.KRW.balance).toLocaleString() + " | 비코: " + Math.floor(currentBalance.BTC.buyingPrice).toLocaleString() + " | 총합 : " + Math.floor(totalBTCKRW).toLocaleString() ) ;
                
                console.log( "현재 BTC 보유 비중 : " + currentBTCRatio + " -> " + BTCRatio + " | " + difRatio ) ;
                
                // 이 부분은 전체 평가금액으로 변경할 것.
                if( totalBTCKRW > 2000000 )
                {
                    if( currentBTCRatio < 0.4 || currentBTCRatio > 0.8 )
                    {
                        var changeOrder = await upbitAPI.changeRatio( difRatio ) ;
        
                        if( changeOrder !== false )
                        {

                            console.log( colors.yellow( changeOrder.phase + " | " + changeOrder.coinName + " | " + changeOrder.price + " | " + changeOrder.size ) ) ;
                            console.log( "BTC 비중 변경" ) ;
                            var uuid = await upbitAPI.order( changeOrder.side , changeOrder.market , changeOrder.price , changeOrder.size ) ;
                
                            logger.info( totalBalance + " | " + changeOrder.phase + " | " + changeOrder.coinName + " | " + changeOrder.price + " | " + changeOrder.size ) ;

                            if( uuid !== false )
                            {
                                // 주문과 동시에 삭제 안시킴.
                                // await upbitAPI.deleteOrder( uuid )
                                callback( 1 ) ;
                                return ;
                            }
                            else
                            {
                                // 오더 에러.
                                console.log( "BTC 비중 변경 판매 에러." ) ;
                                callback( -1 ) ;
                                return ;
                            }
                        }
                        else
                        {
                            // 비중 변경 필요없음.
                            callback( 0 ) ;
                            return ;
                        }
                    }
                }
            }

            // 잉여코인 수익없음.
            callback( 0 ) ;
            return ;
        }

    } , orderDelay ) ;
}


var targetCoin = [ 'BCC' ,
                   'ETH' ,
                   'DASH' ,
                   'ZEC' ,
                   'XMR' ,
                   'REP' ,
                   'BTG' ,
                   'LTC' ,
                   'NEO' ,
                   'QTUM' ,
                   'ETC' ,
                   'LSK' ,
                   'OMG' ,
                   'STRAT' ,
                   'PIVX' ,
                   'WAVES' ,
                   'SBD' ,
                   'KMD' ,
                   'ARK' ,
                   'VTC' ,
                   'STEEM' ,
                   'STORJ' ,
                   'XRP' ,
                   'TIX' ,
                   'POWR' ,
                   'ARDR' ,
                   'GRS' ,
                   'XEM' ,
                   'EMC2' ,
                   'ADA' ,
                   'XLM' ,
                   'TRX' ,
                   'MER' ,
                   'GNT' ,
                   'MCO' ,
                   'SNT' ] ;

// monitoring server 연결
mClient.ConnectMServer() ;

// upbit web socket 연결
upbitAPI.ConnectWebSocket( targetCoin , async function( status ) {
    if( status === false )
    {
        console.log( "WebSocket Connect Failed" ) ;
        process.exit(1) ; 
    }

    console.log( "WebSocket Connect Success!" ) ;

    await upbitAPI.setAPI() ;

    
    // WS 으로 부터 Coin 가격 정보를 모두 받아올 대까지 대기
    upbitAPI.checkCoinPriceMap( targetCoin , async function() {
        console.log( targetCoin ) ;
        console.log( "Coin Price Map Check Success!" ) ;

        // Token 확인
        // if( await upbitAPI.renewToken() === true )
        // {
        //     await upbitAPI.setAPI() ;
        // }

        // 현재 계정의 잔고 갱신.
        if( await upbitAPI.setCurrentBalance() === false )
        {
            console.log( "스타팅 잔고 체크 실패. 프로그램 종료." ) ;
            process.exit(1) ;
        }
        
        // 차익 계산.
        upbitAPI.setCoinProfitMap( targetCoin ) ;

        // bot start.
        setTimeout( async function () {
            setInterval( function() {
                upbitAPI.getMostProfitCase( async function( profitCase ) {
                    // 주문이 미진행일 때에만 계산.
                    if( ( 0 === orderState ) && ( profitCase !== false ) )
                    {
                        // console.log( phase ) ;

                        // 잔고 갱신.
                        if( await upbitAPI.setCurrentBalance() === true )
                        {
                            var currentBalance = await upbitAPI.getCurrentBalance() ;
                            totalBalance = Math.floor( currentBalance.total ).toLocaleString() ;

                            if( currentBalance.BTC.rate < ( -0.005 ) )
                                BTCRatio = 0.2 ;
                            else
                                BTCRatio = 0.5 ;

                            // console.log( 'KRW: ' , currentBalance.KRW.balance ) ;
                            // console.log( 'BTC: ' , currentBalance.BTC.balance ) ;

                            // most profit case 가 phase 1 일 경우.
                            if( ( profitCase.phase === 1 ) && ( profitCase.profit > 0 ) )
                            {
                                // console.log( "Phase 1 !" ) ;
                                if( currentBalance.KRW.balance > profitCase.need )
                                {
                                    // console.log( "balance check !" ) ;
                                    // console.log( new Date().getTime() - phase1.timestamp ) ;
                                    if( ( new Date().getTime() - profitCase.timestamp ) < 30000 )
                                    {
                                        pushOrder( profitCase , function( result ) {
                                            if( result === true )
                                            {
                                                console.log( "Current KRW: " , currentBalance.KRW.balance ) ;
                                                goOrder( function() {
                                                    console.log( "주문 진행 완료\n" ) ;
                                                })
                                            }
                                        } )
                                    }
                                }
                            }
                            // most profit case 가 phase 2 일 경우.
                            else if( ( profitCase.phase === 2 ) && ( profitCase.profit > 0 )  )
                            {
                                // console.log( "Phase 2 !" ) ;
                                if( currentBalance.BTC.balance > profitCase.need )
                                {
                                    // console.log( "balance check !" ) ;
                                    // console.log( new Date().getTime() - phase2.timestamp ) ;

                                    if( ( new Date().getTime() - profitCase.timestamp ) < 30000 )
                                    {
                                        pushOrder( profitCase , function( result ) {
                                            if( result === true )
                                            {
                                                console.log( "Current BTC: " , currentBalance.BTC.balance ) ;
                                                goOrder( function() {
                                                    console.log( "주문 진행 완료\n" ) ;
                                                })
                                            }
                                        } )
                                    }
                                }
                            }                    
                        }
                    }
                } ) ;
            }, 50 ) ;
        }, 1500 ) ;


        setInterval( function() {
            if( orderState === 0 )
            {
                orderState = 1 ; 
                console.log( "현재 주문 없음 손절 / 익절 / BTC 비중 체크" ) ;  
                // 익절 모드 나머지 주문.   
                goRemainedOrder( 1 , function( result ) {
                     // 잉여코인 탐색 시 수익이 없음. 다음 주문을 위해 1.4초 기다림.
                    console.log( "코인 손절 / 익절 / BTC 비중 변경 체크 완료.\n" ) ;
                    if( result !== 0 )
                    {
                        setTimeout( function() {
                            orderState = 0 ;
                        } , orderDelay ) ;
                    }
                    else
                    {
                        // goRemainedOrder 에서 1.4초를 기다렸기 때문에 바로 콜백.
                        orderState = 0 ;
                    }
                }) ;       
            }
        } , 1000 * 5 )
        


        // 일정시간 이상 미체결된 주문들 제거.
        setInterval( async function() {
            var orderList = await upbitAPI.getOrderList() ;
            if( orderList !== false )
            {
                console.log( "매수 주문 및 3분 이상 미체결 매도주문 모두 취소." ) ;
                for( var i = 0 ; i < orderList.length ; ++i )
                {
                    var orderTimeMili = new Date( orderList[i].time ).getTime() ;
                    // 1분 단위
                    var timeGap = ( new Date().getTime() - orderTimeMili ) / ( 1000 * 60 ) ;
            
                    // 매도 주문은 3분 경과 시 삭제.
                    if( (orderList[i].side === 'ask') && (timeGap > 3) )
                    {
                        console.log( orderList[i].side , "|" , orderList[i].market , '|' , orderList[i].time , '|' , Math.floor(timeGap) ) ;
                        await upbitAPI.deleteOrder( orderList[i].uuid ) ;
                    }
                    // 매수주문은 보이는 즉시 삭제.
                    else if( orderList[i].side === 'bid' )
                    {
                        console.log( orderList[i].side , "|" , orderList[i].market , '|' , orderList[i].time , '|' , Math.floor(timeGap) ) ;
                        await upbitAPI.deleteOrder( orderList[i].uuid ) ;
                    }
                }
                console.log( "" ) ;
            }
        }, 1000 * 29 ) ;

        
        // 현재 잔고 및 토큰정보 monitoring server 로 전송.
        setInterval( async function() {

            var currentRenewCount = await upbitAPI.getRenewCount() ;

            if( currentRenewCount !== renewCount )
            {
                renewCount = currentRenewCount ;
                var tokenData = await upbitAPI.getCurrentToken() ;
                mClient.SendAuth( tokenData ) ;
                logger.info( "Token Renew | " + currentRenewCount + " | " + tokenData.token + " | " + tokenData.refreshToken ) ;
            }

            // 잔고 갱신.
            if( await upbitAPI.setCurrentAssets() !== false )
            {
                // 잔고 정보 전송.
                mClient.SendBalance( await upbitAPI.getCurrentBalance() ) ;
            }
            
        } , 3000 ) ;


        // 1분 마다 monitoring server 와 연결 체크.
        setInterval( function() {
            mClient.getConnectStatus( function( connectStatus ) {
                if( false === connectStatus )
                {
                    console.log( "Monitoring Server 재접속 시도." ) ;
                    // monitoring server 재연결
                    mClient.ConnectMServer() ;
                }
            }) ;
        } , 1000 * 60 ) ;
    }) ;
} ) ;